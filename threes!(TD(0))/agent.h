#pragma once
#include<math.h>
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include "weight.h"

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return b.is_win(); }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};
/**
 * base agent for agents with weight tables
 */
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if (meta.find("init") != meta.end()) // pass init=... to initialize the weight
			init_weights(meta["init"]);

		if (meta.find("load") != meta.end()) // pass load=... to load from a specific file
			load_weights(meta["load"]);
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~weight_agent() {
		if (meta.find("save") != meta.end()) // pass save=... to save to a specific file
			save_weights(meta["save"]);
	}

protected:
	virtual void init_weights(const std::string& info) {
		net.emplace_back(65536); // create an empty weight table with size 65536
		net.emplace_back(65536); // create an empty weight table with size 65536
		// now net.size() == 2; net[0].size() == 65536; net[1].size() == 65536
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) { std::exit(-1); std::cout << "error"; }
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
	float alpha;
};

/**
 * base agent for agents with a learning rate
 */
class learning_agent : public agent {
public:
	learning_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~learning_agent() {}

protected:
	float alpha;
};

/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args), space({ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 }), bag({1,2,3}),next_take(3) {}
	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		switch (board(after).before_code())
		{
		case 0:
			for (int pos : space) {
				if (after(pos / 4 + 12) != 0) continue;
				if (next_take >= 3 || after.check_new_game())
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				board::cell tile = bag[next_take++];

				return action::place(pos / 4 + 12, tile);
			}
			return action();
		case 1:
			for (int pos : space) {
				if (after(pos / 4 * 4) != 0) continue;
				if (next_take >= 3 || after.check_new_game())
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				board::cell tile = bag[next_take++];

				return action::place(pos / 4 * 4, tile);
			}
			return action();
		case 2:
			for (int pos : space) {
				if (after(pos / 4) != 0) continue;
				if (next_take >= 3 || after.check_new_game())
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				board::cell tile = bag[next_take++];

				return action::place(pos / 4, tile);
			}
			return action();
		case 3:
			for (int pos : space) {
				if (after(pos / 4 * 4 + 3) != 0) continue;
				if (next_take >= 3 || after.check_new_game())
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				board::cell tile = bag[next_take++];

				return action::place(pos / 4 * 4 + 3, tile);
			}
			return action();
		default:
			for (int pos : space) {
				if (after(pos) != 0) continue;
				if (next_take >= 3 || after.check_new_game())
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				board::cell tile = bag[next_take++];
				return action::place(pos, tile);
			}
			return action();
		}
			/*if (board(after).before_code() != 0 && board(after).before_code() != 1 && board(after).before_code() != 2 && board(after).before_code() != 3) {  return action::place(pos, tile); }
			else
			{
				return action::place(pos / 4, tile);
			}*/
	}
private:
	std::array<int, 16> space;
	std::array<board::cell, 3> bag;
	int next_take;
};

/**
 * dummy player
 * select a legal action randomly
 */
class player : public weight_agent {
public:
	player(const std::string& args = "") : weight_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }) {}

	virtual action take_action(const board& before) {
		//if (record_index[0]==-1) { index_calculation(before);}
		int operation = 0;
		int max_value = -2147483647;
		std::array <int, 8> record_index;
		board::reward max_reward = -1;
		for (int op : opcode) {
			board b = before;
			board::reward reward = b.slide(op);
			//std::cout << b;
			if (reward == -1) continue;
			std::array <int, 8> index = index_calculation(b);
			double value = value_function(index);
			if (max_value < value+reward)
			{
				max_value = value+reward;
				operation = op;
				max_reward = reward;
				record_index = index;
			}
		}
		if (max_reward!=-1)
		{
			reward_stack.emplace_back(max_reward);
			index_stack.emplace_back(record_index);
			return action::slide(operation);
		}
		std::array <int, 8> weight_index = index_stack.back();
		update(weight_index);

		while (true)
		{
			board::reward op_reward = reward_stack.back();
			std::array<int, 8> after_index = index_stack.back();
			index_stack.pop_back();
			if (index_stack.empty()) break;
			reward_stack.pop_back();
			weight_index = index_stack.back();
			update(weight_index, after_index, op_reward);
		}
		reward_stack.clear();
		return action();
	}
	/*double value_function(board b)
	{
		double sum = 0;
		for (int k = 0; k < 2; k++)
			for (int i = 0; i < 4; i++)
			{
				int index = 0;
				for (int j = 0; j < 4; j++)
					index += b.operator()((i << (k << 1)) + (j << (2 - (k << 1)))) << (j << 2);
				sum += net[k][index];
			}
		return sum;
	}*/
	double value_function(std::array <int, 8> index)
	{
		double sum = 0.0;
		sum = net[0][index[0]] + net[1][index[1]] + net[1][index[2]] + net[0][index[3]] + net[0][index[4]] + net[1][index[5]] + net[1][index[6]] + net[0][index[7]];
		return sum;
	}
	std::array <int, 8> index_calculation(board b)
	{
		//std::cout << "------------------------------------";
		std::array <int, 8> record;
		for (int k = 0; k < 2; k++)
			for (int i = 0; i < 4; i++)
			{
				int index = 0;
				for (int j = 0; j < 4; j++)
				{
					index += b((i << (k << 1)) + (j << (2 - (k << 1)))) << (j << 2);
					//std::cout << b.operator()((i << (k << 1)) + (j << (2 - (k << 1))))<<',';
				}
				//std::cout << '\n'<<b;

				record[(k << 2) + i] = index;
			}
		return record;
	}
	void update(std::array <int, 8> current_state_index, std::array <int, 8> after_state_index = {-1,-1,-1,-1,-1,-1,-1,-1}, board::reward reward=0)
	{
		double before_net = value_function(current_state_index);
		if (after_state_index[0] == -1)
		{
			net[0][current_state_index[0]] -= alpha/8 * before_net;
			net[1][current_state_index[1]] -= alpha/8 * before_net;
			net[1][current_state_index[2]] -= alpha/8 * before_net;
			net[0][current_state_index[3]] -= alpha/8 * before_net;
			net[0][current_state_index[4]] -= alpha/8 * before_net;
			net[1][current_state_index[5]] -= alpha/8 * before_net;
			net[1][current_state_index[6]] -= alpha/8 * before_net;
			net[0][current_state_index[7]] -= alpha/8 * before_net;
		}
		else
		{
			double after_net= value_function(after_state_index);
			net[0][current_state_index[0]] += alpha/8 * (reward + after_net - before_net);
			net[1][current_state_index[1]] += alpha / 8 * (reward + after_net - before_net);
			net[1][current_state_index[2]] += alpha / 8 * (reward + after_net - before_net);
			net[0][current_state_index[3]] += alpha / 8 * (reward + after_net - before_net);
			net[0][current_state_index[4]] += alpha / 8 * (reward + after_net - before_net);
			net[1][current_state_index[5]] += alpha / 8 * (reward + after_net - before_net);
			net[1][current_state_index[6]] += alpha / 8 * (reward + after_net - before_net);
			net[0][current_state_index[7]] += alpha / 8 * (reward + after_net - before_net);
		}
	}
	void print()
	{
		net[0].print();
		net[1].print();
	}
	void p(std::array<int,8> x)
	{
		for (int i = 0; i < 8; i++)
			std::cout << x[i];
		std::cout << std::endl;
	}
private:
	std::array<int, 4> opcode;
	std::vector<std::array <int, 8>> index_stack;
	std::vector<board::reward> reward_stack;
};
