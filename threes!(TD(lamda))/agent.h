#pragma once
#include<math.h>
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include "weight.h"

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return b.is_win(); }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};
/**
 * base agent for agents with weight tables
 */
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if (meta.find("init") != meta.end()) // pass init=... to initialize the weight
			init_weights(meta["init"]);

		if (meta.find("load") != meta.end()) // pass load=... to load from a specific file
			load_weights(meta["load"]);
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~weight_agent() {
		if (meta.find("save") != meta.end()) // pass save=... to save to a specific file
			save_weights(meta["save"]);
	}

protected:
	virtual void init_weights(const std::string& info) {
		net.emplace_back(65536); // create an empty weight table with size 65536
		net.emplace_back(65536); // create an empty weight table with size 65536
		// now net.size() == 2; net[0].size() == 65536; net[1].size() == 65536
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) { std::exit(-1); std::cout << "error"; }
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
	float alpha;
};

/**
 * base agent for agents with a learning rate
 */
class learning_agent : public agent {
public:
	learning_agent(const std::string& args = "") : agent(args), alpha(0.1f) {
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	virtual ~learning_agent() {}

protected:
	float alpha;
};

/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args), space({ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 }), bag({ 1,1,1,1,2,2,2,2,3,3,3,3 }), next_take(0), popup(0, 21), need_in_bonus(6), bonus_bag({}) {}
	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		board::cell tile = 0;
		board::cell hint;
		if (after.check_new_game())
		{
			need_in_bonus = 6;
			bonus_bag.clear();
			next_take = 0;
			std::shuffle(bag.begin(), bag.end(), engine);
			tile = bag[next_take++];
			//std::cout << "-----------------------------------------------------------\n";
		}
		else if (after.max_val() > need_in_bonus)
		{
			need_in_bonus = after.max_val();
			//std::cout << need_in_bonus - 3<<std::endl;
			bonus_bag.emplace_back(need_in_bonus-3);
		}
		switch (board(after).before_code())
		{
		case 0:
			for (int pos : space) {
				if (after(pos / 4 + 12) != 0) continue;
				if (next_take >= 12)
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				if (tile == 0)
					tile = after.ch_hint();
				if (after.max_val() > 6)
				{
					if (popup(engine))
						hint = bag[next_take++];
					else
					{
						std::shuffle(bonus_bag.begin(), bonus_bag.end(), engine);
						hint = bonus_bag[0];
					}
				}
				else
				{
					hint = bag[next_take++];
				}
				
				return action::place(pos / 4 + 12, tile,hint);
			}
			return action();
		case 1:
			for (int pos : space) {
				if (after(pos / 4 * 4) != 0) continue;
				if (next_take >= 12)
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				if (tile == 0)
					tile = after.ch_hint();
				if (after.max_val() > 6)
				{
					if (popup(engine))
						hint = bag[next_take++];
					else
					{
						std::shuffle(bonus_bag.begin(), bonus_bag.end(), engine);
						hint = bonus_bag[0];
					}
				}
				else
				{
					hint = bag[next_take++];
				}

				return action::place(pos / 4 * 4, tile, hint);
			}
			return action();
		case 2:
			for (int pos : space) {
				if (after(pos / 4) != 0) continue;
				if (next_take >= 12)
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				if (tile == 0)
					tile = after.ch_hint();
				if (after.max_val() > 6)
				{
					if (popup(engine))
						hint = bag[next_take++];
					else
					{
						std::shuffle(bonus_bag.begin(), bonus_bag.end(), engine);
						hint = bonus_bag[0];
					}
				}
				else
				{
					hint = bag[next_take++];
				}
				return action::place(pos / 4, tile,hint);
			}
			return action();
		case 3:
			for (int pos : space) {
				if (after(pos / 4 * 4 + 3) != 0) continue;
				if (next_take >= 12)
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				if (tile == 0)
					tile = after.ch_hint();
				if (after.max_val() > 6)
				{
					if (popup(engine))
						hint = bag[next_take++];
					else
					{
						std::shuffle(bonus_bag.begin(), bonus_bag.end(), engine);
						hint = bonus_bag[0];
					}
				}
				else
				{
					hint = bag[next_take++];
				}
				return action::place(pos / 4 * 4 + 3, tile,hint);
			}
			return action();
		default:
			for (int pos : space) {
				if (after(pos) != 0) continue;
				if (next_take >= 12)
				{
					std::shuffle(bag.begin(), bag.end(), engine);
					next_take = 0;
				}
				if (tile == 0)
					tile = after.ch_hint();
				if (after.max_val() > 6 && !after.check_new_game())
				{
					if (popup(engine))
						hint = bag[next_take++];
					else
					{
						std::shuffle(bonus_bag.begin(), bonus_bag.end(), engine);
						hint = bonus_bag[0];
					}
				}
				else
				{
					hint = bag[next_take++];
				}
				//std::cout << int(tile)<<","<<int(hint)<<std::endl;
				//std::cout << unsigned(action::place(pos, tile, hint)) <<","<<(35U>>1)<< std::endl;
				return action::place(pos, tile,hint);
			}
			return action();
		}
			/*if (board(after).before_code() != 0 && board(after).before_code() != 1 && board(after).before_code() != 2 && board(after).before_code() != 3) {  return action::place(pos, tile); }
			else
			{
				return action::place(pos / 4, tile);
			}*/
	}
private:
	std::array<int, 16> space;
	std::array<board::cell, 12> bag;
	int next_take;
	std::uniform_int_distribution<int> popup;
	board::cell need_in_bonus;
	std::vector<int> bonus_bag;
};

/**
 * dummy player
 * select a legal action randomly
 */
class player : public weight_agent {
public:
	player(const std::string& args = "") : weight_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }) {}

	virtual action take_action(const board& before) {
		//if (record_index[0]==-1) { index_calculation(before);}
		int operation = 0;
		int max_value = -2147483647;
		board::cell hint = before.ch_hint();
		std::array <int, 8> record_index;
		int before_code = before.before_code();
		if (before_code > 3 || before_code < 0) before_code = 0;
		board::reward max_reward = -1;
		for (int op : opcode) {
			board b = before;
			board::reward reward = b.slide(op);
			//std::cout << b;
			if (reward == -1) continue;
			std::array <int, 8> index = index_calculation(b);
			//std::cout << before_code << "," << hint << std::endl;
			double value = value_function(index, hint, before_code);
			if (max_value < value+reward)
			{
				max_value = value+reward;
				operation = op;
				max_reward = reward;
				record_index = index;
			}
		}
		if (max_reward!=-1)
		{
			hint_stack.emplace_back(hint);
			before_stack.emplace_back(before_code);
			reward_stack.emplace_back(max_reward);
			index_stack.emplace_back(record_index);
			return action::slide(operation);
		}
		std::array <int, 8> weight_index = index_stack.back();
		update(weight_index,hint,before_code);
		while (true)
		{
			board::reward op_reward = reward_stack.back();
			board::cell op_hint = hint_stack.back();
			int code = before_stack.back();
			std::array<int, 8> after_index = index_stack.back();
			index_stack.pop_back();
			if (index_stack.empty()) break;
			reward_stack.pop_back();
			before_stack.pop_back();
			hint_stack.pop_back();
			weight_index = index_stack.back();
			update(weight_index, op_hint, code, after_index, op_reward);
		}
		reward_stack.clear();
		hint_stack.clear();
		before_stack.clear();
		return action();
	}
	/*double value_function(board b)
	{
		double sum = 0;
		for (int k = 0; k < 2; k++)
			for (int i = 0; i < 4; i++)
			{
				int index = 0;
				for (int j = 0; j < 4; j++)
					index += b.operator()((i << (k << 1)) + (j << (2 - (k << 1)))) << (j << 2);
				sum += net[k][index];
			}
		return sum;
	}*/
	double value_function(std::array <int, 8> index,board::cell hint,int before_code)
	{
		int first_index=0;
		/*if (hint < 4) first_index = 4 * hint - 4 + before_code;
		else first_index = 8 + before_code;*/
		double sum = 0.0;
		sum = net[first_index][index[0]] + net[first_index+1][index[1]] + net[first_index+1][index[2]] + net[first_index][index[3]] + net[first_index][index[4]] + net[first_index+1][index[5]] + net[first_index+1][index[6]] + net[first_index][index[7]];
		return sum;
	}
	std::array <int, 8> index_calculation(board b)
	{
		//std::cout << "------------------------------------";
		std::array <int, 8> record;
		for (int k = 0; k < 2; k++)
			for (int i = 0; i < 4; i++)
			{
				int index = 0;
				for (int j = 0; j < 4; j++)
				{
					index += b((i << (k << 1)) + (j << (2 - (k << 1)))) << (j << 2);
					//std::cout << b.operator()((i << (k << 1)) + (j << (2 - (k << 1))))<<',';
				}
				//std::cout << '\n'<<b;

				record[(k << 2) + i] = index;
			}
		return record;
	}
	void update(std::array <int, 8> current_state_index, board::cell hint, int before_code, std::array <int, 8> after_state_index = {-1,-1,-1,-1,-1,-1,-1,-1}, board::reward reward=0)
	{
		int first_index=0;
		/*if (hint < 4) first_index = 4* hint - 4 + before_code;
		else first_index = 8 + before_code;*/
		double before_net = value_function(current_state_index, hint, before_code);
		if (after_state_index[0] == -1)
		{
			net[first_index][current_state_index[0]] -= alpha/8 * before_net;
			net[first_index+1][current_state_index[1]] -= alpha/8 * before_net;
			net[first_index+1][current_state_index[2]] -= alpha/8 * before_net;
			net[first_index][current_state_index[3]] -= alpha/8 * before_net;
			net[first_index][current_state_index[4]] -= alpha/8 * before_net;
			net[first_index+1][current_state_index[5]] -= alpha/8 * before_net;
			net[first_index+1][current_state_index[6]] -= alpha/8 * before_net;
			net[first_index][current_state_index[7]] -= alpha/8 * before_net;
		}
		else
		{
			double after_net= value_function(after_state_index,hint,before_code);
			net[first_index][current_state_index[0]] += alpha/8 * (reward + after_net - before_net);
			net[first_index+1][current_state_index[1]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index+1][current_state_index[2]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index][current_state_index[3]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index][current_state_index[4]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index+1][current_state_index[5]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index+1][current_state_index[6]] += alpha / 8 * (reward + after_net - before_net);
			net[first_index][current_state_index[7]] += alpha / 8 * (reward + after_net - before_net);
		}
	}
	void print()
	{
		net[0].print();
		net[1].print();
	}
	void p(std::array<int,8> x)
	{
		for (int i = 0; i < 8; i++)
			std::cout << x[i];
		std::cout << std::endl;
	}
private:
	std::array<int, 4> opcode;
	std::vector<std::array <int, 8>> index_stack;
	std::vector<board::reward> reward_stack;
	std::vector<board::cell> hint_stack;
	std::vector<int> before_stack;
};
