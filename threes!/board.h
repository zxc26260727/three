#pragma once
#include <array>
#include <iostream>
#include <iomanip>

/**
 * array-based board for 2048
 *
 * index (1-d form):
 *  (0)  (1)  (2)  (3)
 *  (4)  (5)  (6)  (7)
 *  (8)  (9) (10) (11)
 * (12) (13) (14) (15)
 *
 */
class board {
public:
	typedef uint32_t cell;
	typedef std::array<cell, 4> row;
	typedef std::array<row, 4> grid;
	typedef uint64_t data;
	typedef int reward;

public:
	board() : tile(), attr(0) {}
	board(const grid& b, data v = 0) : tile(b), attr(v) ,op(-1u){}
	board(const board& b) = default;
	board& operator =(const board& b) = default;

	operator grid&() { return tile; }
	operator const grid&() const { return tile; }
	row& operator [](unsigned i) { return tile[i]; }
	const row& operator [](unsigned i) const { return tile[i]; }
	cell& operator ()(unsigned i) { return tile[i / 4][i % 4]; }
	const cell& operator ()(unsigned i) const { return tile[i / 4][i % 4]; }

	data info() const { return attr; }
	data info(data dat) { data old = attr; attr = dat; return old; }
	unsigned  before_code() const { return op; }

public:
	bool operator ==(const board& b) const { return tile == b.tile; }
	bool operator < (const board& b) const { return tile <  b.tile; }
	bool operator !=(const board& b) const { return !(*this == b); }
	bool operator > (const board& b) const { return b < *this; }
	bool operator <=(const board& b) const { return !(b < *this); }
	bool operator >=(const board& b) const { return !(*this < b); }

public:

	/**
	 * place a tile (index value) to the specific position (1-d form index)
	 * return 0 if the action is valid, or -1 if not
	 */
		/*reward  place(unsigned pos, cell tile){
			if (op == -1u)
			{
				return non_rule_place(pos, tile);
			}
			else
			{
				return rule_place(pos, tile);
			}
		}

		reward rule_place(unsigned pos, cell tile) {
		if (pos >= 4) return -1;
		if (tile != 1 && tile != 2 && tile != 3) return -1;
		switch (op)
		{
		case 0: operator[](3)[pos] = tile; break;
		case 1: operator[](pos)[0] = tile; break;
		case 2: operator[](0)[pos] = tile; break;
		case 3: operator[](pos)[3] = tile; break;
		default: return -1;
		}
		return 0;
	}*/
		reward place(unsigned pos, cell tile)
		{
			if (pos >= 16) return -1;
			if (tile != 1 && tile != 2 && tile != 3) return -1;
			operator()(pos) = tile;
			return 0;
		}
		reward check_new_game() const{
			for (int i = 0; i < 16; i++)
				if (operator()(i) != 0)
					return 0;
			return 1;
		}
	/**
	 * apply an action to the board
	 * return the reward of the action, or -1 if the action is illegal
	 */
	reward slide(unsigned opcode) {
		op = opcode & 0b11;
		switch (opcode & 0b11) {
		case 0: return slide_up();
		case 1: return slide_right();
		case 2: return slide_down();
		case 3: return slide_left();
		default: return -1;
		}
	}

	reward slide_left() {
		board prev = *this;
		reward score = 0;
		for (int r = 0; r < 4; r++) {
			auto& row = tile[r];
			int hold = row[0];
			for (int c = 1; c < 4; c++) {
				int tile = row[c];
				if (tile == 0) {
					hold = 0;
					continue;
				}
				if (hold >= 3) {
					if (tile == hold) {
						row[c-1] = ++tile;
						row[c] = 0;
						score += ((3 ^ (tile - 3)));
					}
				}
				else if (!hold) {
					row[c-1] = tile;
					row[c] = 0;
				}
				else if (tile + hold == 3) {
					row[c-1] = 3;
					row[c] = 0;
					score += 3;
				}
				hold = row[c];
			}
		}
		return (*this != prev) ? score : -1;
	}
	reward slide_right() {
		reflect_horizontal();
		reward score = slide_left();
		reflect_horizontal();
		return score;
	}
	reward slide_up() {
		rotate_right();
		reward score = slide_right();
		rotate_left();
		return score;
	}
	reward slide_down() {
		rotate_right();
		reward score = slide_left();
		rotate_left();
		return score;
	}

	void transpose() {
		for (int r = 0; r < 4; r++) {
			for (int c = r + 1; c < 4; c++) {
				std::swap(tile[r][c], tile[c][r]);
			}
		}
	}

	void reflect_horizontal() {
		for (int r = 0; r < 4; r++) {
			std::swap(tile[r][0], tile[r][3]);
			std::swap(tile[r][1], tile[r][2]);
		}
	}

	void reflect_vertical() {
		for (int c = 0; c < 4; c++) {
			std::swap(tile[0][c], tile[3][c]);
			std::swap(tile[1][c], tile[2][c]);
		}
	}

	/**
	 * rotate the board clockwise by given times
	 */
	void rotate(int r = 1) {
		switch (((r % 4) + 4) % 4) {
		default:
		case 0: break;
		case 1: rotate_right(); break;
		case 2: reverse(); break;
		case 3: rotate_left(); break;
		}
	}

	void rotate_right() { transpose(); reflect_horizontal(); } // clockwise
	void rotate_left() { transpose(); reflect_vertical(); } // counterclockwise
	void reverse() { reflect_horizontal(); reflect_vertical(); }

public:
	friend std::ostream& operator <<(std::ostream& out, const board& b) {
		out << "+------------------------+" << std::endl;
		for (auto& row : b.tile) {
			out << "|" << std::dec;
			for (auto t : row)
				if (t < 3)
					out << std::setw(6) << t;
				else
					out << std::setw(6) << (1<<(t-2))+(1<<(t-3));
			out << "|" << std::endl;
		}
		out << "+------------------------+" << std::endl;
		return out;
	}

private:
	grid tile;
	data attr;
	unsigned op=-1u;
};
